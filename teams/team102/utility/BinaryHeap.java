package team102.utility;

public class BinaryHeap <AnyType> {

    private static final int DEFAULT_CAPACITY = 10;

    private int currentSize;      // Number of elements in heap
    private AnyType[] array; // The heap array
    private int[] num; // The heap array
    /**
     * Construct the binary heap.
     */
    public BinaryHeap() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Construct the binary heap.
     * @param capacity the capacity of the binary heap.
     */
    public BinaryHeap( int capacity ) {
        currentSize = 0;
        array = (AnyType[]) new Object[ capacity + 1 ];
        num = new int[ capacity + 1 ];
    }
    
    /**
     * Insert into the priority queue, maintaining heap order.
     * Duplicates are allowed.
     * @param x the item to insert.
     */
    public void add( AnyType x , int number) {
//   assume array is large enough
//        if( currentSize == array.length - 1 )
//            enlargeArray( array.length * 2 + 1 );

        // Percolate up
        int hole = ++currentSize;
        while (hole > 1 && number < num[hole/2]) {
        	array[hole] = array[hole/2];
        	num[hole] = num[hole/2];
        	hole /= 2;
        }
        array[hole] = x;
        num[hole] = number;
    }


    /**
     * Find the smallest item in the priority queue.
     * @return the smallest item, or throw an UnderflowException if empty.
     */
    public AnyType peek() {
        return array[ 1 ];
    }

    /**
     * Remove the smallest item from the priority queue.
     * @return the smallest item, or throw an UnderflowException if empty.
     */
    public AnyType poll() {
        AnyType minItem = array[1];
        AnyType temp = array[1] = array[ currentSize ];
        int tmp = num[1] = num[ currentSize--];
        int hole = 1;
        int child = 2; 

        while (child <= currentSize)
        {
            if(child != currentSize && num[child+1] < num[child])
                child++;
            if(num[child] < tmp) {
                array[ hole ] = array[ child ];
                num[hole] = num[child];
            } else {
                break;
            }
            hole = child;
            child = hole*2;
        }
        array[ hole ] = temp;
        num[hole] = tmp;

        return minItem;
    }
    /**
     * Test if the priority queue is logically empty.
     * @return true if empty, false otherwise.
     */
    public boolean isEmpty( )
    {
        return currentSize == 0;
    }

    /**
     * Make the priority queue logically empty.
     */
    public void makeEmpty( )
    {
        currentSize = 0;
    }
    
    public int size() {
    	return currentSize;
    }
    // Test program
    public static void main( String [] args )
    {
    	int numItems = 10000;
    	BinaryHeap<Integer> h = new BinaryHeap<Integer>(100000);
    	int i = 37;

    	for( i = 37; i != 0; i = ( i + 37 ) % numItems )
    		h.add( i,i );
    	for( i = 1; i < numItems; i++ )
    		if( h.poll( ) != i )
    			System.out.println( "Oops! " + i );
		System.out.println( "WORKS! " );
    }
}
