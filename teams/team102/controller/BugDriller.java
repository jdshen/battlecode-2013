package team102.controller;

import team102.utility.FastLocSet;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class BugDriller {

	
	public static enum HugState {
		MOVE, HUG_ONE, HUG_TWO, DRILL_ONE, DRILL_TWO
	}
	
	protected HugState hugging; //0 if not hugging, 1 if hugging one dir, 2 if hugging second
	private boolean hugLeft; //hug left means that bot went left (so wall to the right)
	private FastLocSet seen;
	private MapLocation mid;
	private MapLocation to;
	
	private Direction hugDir;
	private Direction desiredDir;
	private RobotController rc;
	private SensingRobotState rs;
	
	private MapLocation loc;
	private Team enemy;

	public MapLocation from; //original starting point
	public Direction next;
	public boolean mine;
		
	private static final Direction[][] BEST_DIR = getBestDir();
	
	private static final SecondStep[][] BEST_2STEP = getBestSecondSteps();
			
	private static class SecondStep {
		public final int x;
		public final int y;
		public final Direction[] d;
		public SecondStep(int x, int y, Direction[] d) {
			this.x = x;
			this.y = y;
			this.d = d;
		}
		
		@Override
		public String toString() {
			String s = "";
			for (Direction dir : d) {
				s += dir+",";
			}
			
			return x + ","+ y + ";" + s;
		}
	}
	
	
	public BugDriller(SensingRobotState rs, RobotController rc) {
		this.rs = rs;
		this.rc = rc;
	}
	
	public void startBug(MapLocation to) {
		this.from = rs.loc;
		this.mid = from;
		this.to = to;
		hugging = HugState.MOVE;
		hugLeft = true;
		seen = new FastLocSet();
	}
	
	/**
	 * Uses bugging algorithm to find next move, tells you if your next move goes through a mine
	 * .next stores next move, .mine is true iff there is a mine 
	 * @throws GameActionException 
	 * @paam loc current location
	 */
	public void bug() throws GameActionException {
		update();
		
		desiredDir = loc.directionTo(to);
		if (desiredDir == Direction.NONE || desiredDir == Direction.OMNI) {
			next = desiredDir;
			mine = false;
			return;
		}

		//try to do normal action
		switch(hugging) {
		case MOVE:
			if (checkMove()) {
				return;
			}
			break;
		case HUG_ONE:
		case HUG_TWO:
			//check if seen already, otherwise, fall into Drill_TWO
			if (seen.contains(loc)) {
				hugging = HugState.DRILL_TWO;
				break;
			}			
			//see if can move closer than started
			if (checkHugMove()) {
				hugging = HugState.MOVE;
				return;
			}
			//try to hug
			if (hug()) {
				seen.add(loc);
				return;
			}
			break;
		}
		
		//didn't work, fall into next option
		switch(hugging) {
		case MOVE:
			//moving didnt work, try to hug one
			hugDir = desiredDir;
			hugLeft = true;
			if (hug()) { 
				seen.clear();
				seen.add(loc);
				hugging = HugState.HUG_ONE;
				mid = loc;
				return;
			}
			//fall into hug one
			
		case HUG_ONE:
			//hugging left didnt work, hug two
			hugLeft = false;
			if (hug()) {
				seen.clear();
				seen.add(loc);
				hugging = HugState.HUG_TWO;
				return;
			}
			//fall into hug two
			
		case HUG_TWO:
		case DRILL_ONE:
			//hugging two didnt work OR we are on drill one, try to drill
			if (drill()) {
				hugging = HugState.DRILL_TWO;
				return;
			}
			//drilling didnt work, fall into flailing around
			
		case DRILL_TWO:
			//try to drill two
			if (drill()) {
				hugging = HugState.MOVE;
				return;
			}
			//drilling didnt work, fall into flailing	
		
		default:
			if (flail()) {
				hugging = HugState.MOVE;
				return;
			} else {
				hugging = HugState.MOVE;
				next = Direction.NONE;
				mine = false;
				return;
			}
		}
	}
	
	private void update() {
		loc = rs.loc;
		enemy = rs.enemy;
	}
	
	private boolean drill() {
		//check one step ahead
		Direction[] best = BEST_DIR[desiredDir.ordinal()];
		for (Direction d : best) {
			if (rc.canMove(d)) {
				next = d;
				mine = isMine(loc.add(d));
				return true;
			}
		}
		return false;
	}
	

	private boolean flail() {
		//check safe moves
		for (int i = 0; i < 8; i++) {
			Direction d = Direction.values()[i];
			if (safeMove(d)) {
				next = d;
				mine = false;
				return true;
			}
		}
		//check any move
		for (int i = 0; i < 8; i++) {
			Direction d = Direction.values()[i];
			if (rc.canMove(d)) {
				next = d;
				mine = true;
				return true;
			}
		}
		return false;
	}
	
	private boolean hug() {
		//try to move in hugDir, rotate until can move: this hugs the hugDir
	    Direction tryDir = hugDir;
	    int i = 0;
	    boolean offMap = false;
	    for (i = 0; i < 8 && !safeMove(tryDir); i++){
	    	if (rc.senseTerrainTile(loc.add(tryDir)) == TerrainTile.OFF_MAP) {
	    		offMap = true;
	    		break;
	    	}
	        tryDir = hugLeft ? tryDir.rotateLeft() : tryDir.rotateRight();
	    }
	    
	    if (offMap) {
	    	//reached end of map
	    	return false;
	    } else if (i == 8) {
	    	//blocked in all directions
	    	next = Direction.NONE;
	    	mine = false;
	    	return true;
	    	
	    } else {
	    	//hug successful
			hugDir = tryDir;
	    	if (hugDir.isDiagonal()) {
	    		hugDir = hugLeft ? hugDir.rotateRight().rotateRight().rotateRight()
	    				: hugDir.rotateLeft().rotateLeft().rotateLeft();
	    	} else {
		    	hugDir = hugLeft ? hugDir.rotateRight().rotateRight() 
		    			: hugDir.rotateLeft().rotateLeft();
	    	}
	    	next = tryDir;
	    	mine = false;
	    	return true;
	    }
	}
	
	private boolean checkMove() throws GameActionException {
		//check one step ahead
		Direction[] best = BEST_DIR[desiredDir.ordinal()];
		for (Direction d : best) {
			if (safeMove(d)) {
				next = d;
				mine = false;
				return true;
			}
		}
		
		//check two steps
		SecondStep[] secondStep = BEST_2STEP[desiredDir.ordinal()];
		for (SecondStep s : secondStep) {
			//check second step is safe
			if (safeMove(loc.add(s.x,s.y))){
				//see if can get to second step
				for (Direction dir : s.d) {
					if (isMine(loc.add(dir))) {
						next = dir;
						mine = true;
						return true;
					}
				}
			}
		}
		
		return false;
	}
	

	private boolean checkHugMove() throws GameActionException {
		//check one step ahead
		Direction[] best = BEST_DIR[desiredDir.ordinal()];
		for (Direction d : best) {
			//check if safe move, and brings closer than beginning of hug
			if (safeMove(d) && loc.distanceSquaredTo(to) <= mid.distanceSquaredTo(to)) {
				next = d;
				mine = false;
				return true;
			}
		}
		
		//check two steps
//		SecondStep[] secondStep = BEST_2STEP[desiredDir.ordinal()];
//		for (SecondStep s : secondStep) {
//			//check second step is safe, and if better than going back to beginning of hug
//			if (safeMove(loc.add(s.x,s.y)) && secondStepMetric()){
//				System.out.println(loc.add(s.x,s.y));
//				System.out.println(s.x+" "+s.y);
//				//see if can get to second step
//				for (Direction dir : s.d) {
//					if (isMine(loc.add(dir))) {
//						next = dir;
//						mine = true;
//						return true;
//					}
//				}
//			}
//		}
		
		return false;
	}
	
	private boolean secondStepMetric() {
		return true;
//		return loc.distanceSquaredTo(to) + defuseDelay
//				<= mid.distanceSquaredTo(to) + loc.distanceSquaredTo(mid);
	}
	
	private boolean safeMove(Direction d) {
		Team t = rc.senseMine(loc.add(d));
		return rc.canMove(d) && t != enemy && t != Team.NEUTRAL;
	}
	
	private boolean isMine(MapLocation m) {
		Team t = rc.senseMine(m);
		return (t == enemy || t == Team.NEUTRAL);
	}
	
	private boolean canMove(MapLocation m) throws GameActionException {
		return rc.senseTerrainTile(m) == TerrainTile.LAND && rc.senseObjectAtLocation(m) == null;
	}
	
	private boolean safeMove(MapLocation m) throws GameActionException {
//		boolean hasMine = rs.mines.contains(m);
		Team t = rc.senseMine(m);
		return rc.senseTerrainTile(m) == TerrainTile.LAND && rc.senseObjectAtLocation(m) == null
//				&& hasMine && hasMine2;
				&& t != enemy && t != Team.NEUTRAL;
	}
	
	private static Direction[][] getBestDir() {
		Direction[][] ret = new Direction[Direction.values().length][];
		for (Direction d : Direction.values()) {
			Direction[] best = new Direction[3];
			best[0] = d;
			best[1] = d.rotateLeft();
			best[2] = d.rotateRight();
			ret[d.ordinal()] = best;
		}
		return ret;
	}
	
	private static SecondStep[][] getBestSecondSteps() {
		SecondStep[][] list = new SecondStep[Direction.values().length][];
		SecondStep[] s = new SecondStep[16];
		s[0] = new SecondStep(-2,-2,new Direction[]{Direction.NORTH_WEST});
		s[1] = new SecondStep(-1,-2,new Direction[]{Direction.NORTH_WEST, Direction.NORTH});
		s[2] = new SecondStep(0,-2,new Direction[]{Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST});
		s[3] = new SecondStep(1,-2,new Direction[]{Direction.NORTH, Direction.NORTH_EAST});
		s[4] = new SecondStep(2,-2,new Direction[]{Direction.NORTH_EAST});
		s[5] = new SecondStep(2,-1,new Direction[]{Direction.NORTH_EAST, Direction.EAST});
		s[6] = new SecondStep(2,0,new Direction[]{Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST});
		s[7] = new SecondStep(2,1,new Direction[]{Direction.EAST, Direction.SOUTH_EAST});
		s[8] = new SecondStep(2,2,new Direction[]{Direction.SOUTH_EAST});
		s[9] = new SecondStep(1,2,new Direction[]{Direction.SOUTH_EAST, Direction.SOUTH});
		s[10] = new SecondStep(0,2,new Direction[]{Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST});
		s[11] = new SecondStep(-1,2,new Direction[]{Direction.SOUTH, Direction.SOUTH_WEST});
		s[12] = new SecondStep(-2,2,new Direction[]{Direction.SOUTH_WEST});
		s[13] = new SecondStep(-2,1,new Direction[]{Direction.SOUTH_WEST, Direction.WEST});
		s[14] = new SecondStep(-2,0,new Direction[]{Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST});
		s[15] = new SecondStep(-2,-1,new Direction[]{Direction.WEST, Direction.NORTH_WEST});

		//for each possible direction
		int best = 2; //north is first
		for (Direction d : Direction.values()) {
			if (d == Direction.OMNI || d == Direction.NONE) {
				list[d.ordinal()] = new SecondStep[0];
				continue;
			}
			//add up every pair of directions and add to secondStep
			SecondStep[] secondStep = new SecondStep[5];
			secondStep[0] = s[best%16];
			secondStep[1] = s[(best+1)%16];
			secondStep[2] = s[(best-1)%16];
			secondStep[3] = s[(best+2)%16];
			secondStep[4] = s[(best-2)%16];
			best+=2;
			list[d.ordinal()] = secondStep;
		}
		return list;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < Direction.values().length; i++) {
			Direction d = Direction.values()[i];
			SecondStep[] arr = BEST_2STEP[d.ordinal()];
			for (SecondStep s : arr) {
				System.out.println(d+ " " + s);
			}
		}
		
		Direction d= Direction.EAST;
		System.out.println(BEST_2STEP[d.ordinal()][2].x + " "+BEST_2STEP[d.ordinal()][2].y);
	}
}
