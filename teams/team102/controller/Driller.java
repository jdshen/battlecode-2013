package team102.controller;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class Driller {
	private RobotController rc;
	private SensingRobotState rs;

	private MapLocation to;
	
	public MapLocation from;
	public Direction next;
	public boolean mine;

	private static final int INFINITY  = 2000000;
	private static final Direction[][] BEST_DIR = getOrderedDir();

	public Driller(SensingRobotState rs, RobotController rc) {
		this.rs = rs;
		this.rc = rc;
	}
	
	public void startDrill(MapLocation to) {
		this.from = rs.loc; 
		this.to = to;
		this.next = null;
	}
	
	public void drill(int drillSquaredDistance) {
		Direction desiredDir = rs.loc.directionTo(to);
		if (desiredDir == Direction.NONE || desiredDir == Direction.OMNI) {
			next = desiredDir;
			mine = false;
			return;
		}
		
		Direction[] best = BEST_DIR[desiredDir.ordinal()];
		Direction bestDir = null;
		int bestDist = 0;
		//check each direction, see how far you can go without running into a mine.
		for (int i = 0; i < 3; i++) {
			Direction d = best[i];
			if (safeMove(d)) {
				MapLocation curLoc = rs.loc;
				int curDistance = to.distanceSquaredTo(curLoc);
				int j = 0;
				
				MapLocation nextLoc;
				int nextDistance;
				while (true) {
					nextLoc = curLoc.add(d);
					//break when mine
					if (isMine(nextLoc)) {
						break;
					}
					
					nextDistance = to.distanceSquaredTo(nextLoc);
					if (nextDistance >= curDistance || (j+1)*(j+1) > drillSquaredDistance) {
						break;
					}
					
					curLoc = nextLoc;
					curDistance = nextDistance;
					j++;
				}
				
				if (j > bestDist) {
					bestDir = d;
					bestDist = j;
				}
			}
		}
		
		if (bestDir != null) {
			next =  bestDir;
			mine = false;
			return;
		}
		
		bestDist = INFINITY;
		for (int i = 0; i < 3; i++) {
			Direction d = best[i];
			if (rc.canMove(d)) {
				MapLocation curLoc = rs.loc;
				int curDistance = to.distanceSquaredTo(rs.loc);
				int j = 0;
				
				MapLocation nextLoc;
				int nextDistance;
				while (true) {
					nextLoc = curLoc.add(d);
					//break when NOT mine
					if (!isMine(nextLoc)) {
						break;
					}
					
					nextDistance = to.distanceSquaredTo(nextLoc);
					if (nextDistance > curDistance || (j+1)*(j+1) > drillSquaredDistance) {
						break;
					}
					
					curLoc = nextLoc;
					curDistance = nextDistance;
					j++;
				}
				
				if (j < bestDist && j != 0) {
					bestDir = d;
					bestDist = j;
				}
			}
		}
		
		if (bestDir != null) {
			next =  bestDir;
			mine = true;
			return;
		}
		
		next = Direction.NONE;
		mine = false;
	}
	
//	private int distance(MapLocation a, MapLocation b) {
//		return Math.max(Math.abs(a.x - b.x), Math.abs(a.y-b.y));
//	}

	private boolean safeMove(Direction d) {
		Team t = rc.senseMine(rs.loc.add(d));
		return rc.canMove(d) && t != rs.enemy && t != Team.NEUTRAL;
	}
	
	private boolean isMine(MapLocation m) {
		Team t = rc.senseMine(m);
		return (t == rs.enemy || t == Team.NEUTRAL);
	}
	
	private boolean canMove(MapLocation m) throws GameActionException {
		return rc.senseTerrainTile(m) == TerrainTile.LAND && rc.senseObjectAtLocation(m) == null;
	}
	
	private boolean safeMove(MapLocation m) throws GameActionException {
		Team t = rc.senseMine(m);
		return rc.senseTerrainTile(m) == TerrainTile.LAND && rc.senseObjectAtLocation(m) == null
				&& t != rs.enemy && t != Team.NEUTRAL;
	}
	
	private static Direction[][] getOrderedDir() {
		Direction[][] ret = new Direction[Direction.values().length][];
		for (Direction d : Direction.values()) {
			Direction[] best = new Direction[3];
			best[0] = d;
			best[1] = d.rotateLeft();
			best[2] = d.rotateRight();
			ret[d.ordinal()] = best;
		}
		return ret;
	}
}
