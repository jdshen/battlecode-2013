package team102.controller;

import battlecode.common.*;
import team102.utility.*;

public abstract class SensingRobotState extends BasicRobotState{
	//basic info
	protected double energon;
	protected double shields;
	protected double power;
	protected int round;
	
	//location info
	protected MapLocation loc;
	
	//encampment info
	private int encampmentDelay;
	protected MapLocation[] alliedEncampments;
	
	//upgrade info
	protected int defuseDelay;
	protected boolean defusion;
	protected double powerDecay;
	protected boolean fusion;
	protected boolean pickaxe;
	protected int sensorRadiusSquared;
	protected boolean sensor;
	
	//non allied
	protected MapLocation[] nearbyMines;
	protected FastLocSet mines;
	//allied
	protected MapLocation[] nearbyAlliedMines;
	protected FastLocSet alliedMines;
	//enemies
	protected RobotInfo[] nearbyEnemies;
	protected FastLocSet enemies;
	//enemies
	protected RobotInfo[] nearbyAllies;
	protected FastLocSet allies;
	
	protected int numAllies;
	
	
	public SensingRobotState(RobotController rc) throws GameActionException{
		super(rc);
		initSense();
	}
	
	protected void getBasic() {
		energon = rc.getEnergon();
		shields = rc.getShields();
		power = rc.getTeamPower();
		round = Clock.getRoundNum();
	}
	
	protected void getLocation() {
		loc = rc.getLocation();
	}
	
	protected void setEncampmentDelay(int delay) {
		encampmentDelay = delay;
	}
	
	protected void initUpgrades() {
		defuseDelay = GameConstants.MINE_DEFUSE_DELAY;
		defusion = false;
		powerDecay = GameConstants.POWER_DECAY_RATE;
		fusion = false;
		pickaxe = false;
		sensorRadiusSquared = type.sensorRadiusSquared;
		sensor = false;
	}

	protected void getUpgrades() {
		if (!defusion && rc.hasUpgrade(Upgrade.DEFUSION)) {
			defusion = true;
			defuseDelay = GameConstants.MINE_DEFUSE_DEFUSION_DELAY;
		}
		
		if (!fusion && rc.hasUpgrade(Upgrade.FUSION)) {
			fusion = true;
			powerDecay = GameConstants.POWER_DECAY_RATE_FUSION;
		}
		
		pickaxe |= rc.hasUpgrade(Upgrade.PICKAXE);
		
		if (!sensor && rc.hasUpgrade(Upgrade.VISION)) {
			sensor = true;
			sensorRadiusSquared += GameConstants.VISION_UPGRADE_BONUS;
		}
	}
	
	protected void getEncampments() {
		if (round % encampmentDelay == 0) {
			alliedEncampments = rc.senseAlliedEncampmentSquares();
		}
	}
	
	protected void initMines() {
		mines = new FastLocSet();
		alliedMines = new FastLocSet();
	}
	
	protected void getNonAlliedMines(int searchRadiusSquared) {
		nearbyMines = rc.senseNonAlliedMineLocations(loc, searchRadiusSquared);
		mines.clear();
		for (MapLocation m : nearbyMines) {
			mines.add(m);
		}
	}
	
	protected void initObjects() {
		enemies = new FastLocSet();
		allies = new FastLocSet();
	}
	
	protected void getEnemies(int searchRadiusSquared) throws GameActionException{
		Robot[] en = rc.senseNearbyGameObjects(Robot.class, searchRadiusSquared, enemy);
		nearbyEnemies = new RobotInfo[en.length]; 
		enemies.clear();
		for (int i = 0; i < en.length; i++) {
			nearbyEnemies[i] = rc.senseRobotInfo(en[i]);
			enemies.add(nearbyEnemies[i].location);
		}
	}
	
	protected void getAllies(int searchRadiusSquared) throws GameActionException {
		Robot[] al = rc.senseNearbyGameObjects(Robot.class, searchRadiusSquared, team);
		nearbyAllies = new RobotInfo[al.length]; 
		allies.clear();
		for (int i = 0; i < al.length; i++) {
			nearbyAllies[i] = rc.senseRobotInfo(al[i]);
			allies.add(nearbyAllies[i].location);
		}
	}
	
	protected void getNumAllies() {
		Robot[] al = rc.senseNearbyGameObjects(Robot.class, 2000000, team);
		numAllies = al.length;
	}
	
	protected void getAlliedMines(int searchRadiusSquared) {
		nearbyAlliedMines = rc.senseMineLocations(loc, searchRadiusSquared, team);
		alliedMines.clear();
		for (MapLocation m : nearbyAlliedMines) {
			alliedMines.add(m);
		}
	}

	/**
	 * Initializes static sensory information
	 */
	protected abstract void initSense() throws GameActionException;
	
	/**
	 * Updates the information based on sensory input 
	 */
	public abstract void updateSense() throws GameActionException;
}
