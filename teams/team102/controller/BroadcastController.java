package team102.controller;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;

public class BroadcastController {
	private int channel;
	private final static int PASS = 2571; // larger than round num
	private BasicRobotState rs;
	public final static int MAX = GameConstants.BROADCAST_MAX_CHANNELS;
	
	public BroadcastController(BasicRobotState rs) {
		this.rs = rs;
		channel = rs.teamhq.x+rs.teamhq.y;
	}
	
	public void broadcast(int channel, int a) throws GameActionException {
		rs.rc.broadcast(channel, a*PASS+Clock.getRoundNum());
	}
	public boolean read(int channel, int message) throws GameActionException {
		int num = rs.rc.readBroadcast(channel);
		
		int check = Clock.getRoundNum() - (num % PASS);
		if (check != 0 && check != 1){ // at most 1 round old
			return false;
		}
		
		if (message == num/PASS) {
			return true;
		}

		return false;
	}
}
