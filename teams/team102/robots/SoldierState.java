package team102.robots;

import javax.print.attribute.standard.Destination;

import battlecode.common.*;
import battlecode.engine.instrumenter.lang.System;
import team102.controller.*;

public class SoldierState extends SensingRobotState {
	private boolean bugging;
//	private Bugger bugger;
	private Driller driller;
	private BroadcastController bc;
	private boolean wait;
	private static final int stray = 7;
	private MapLocation dest;

	public SoldierState(RobotController rc) throws GameActionException{
		super(rc);
		bugging = false;
//		bugger = new Bugger(this, rc);
		driller = new Driller(this, rc);
		driller.startDrill(teamhq);
		bc = new BroadcastController(this);
		wait = false;
		dest = null;
	}
	
	public void initSense() {
		setEncampmentDelay(10);
		initUpgrades();
		initMines();
		initObjects();
	}
	
	public void updateSense() throws GameActionException {
		getLocation();
		getBasic();
		getEncampments();
		getUpgrades();
//		getNonAlliedMines(sensorRadiusSquared*2);
//		getAlliedMines(sensorRadiusSquared);
		getEnemies(sensorRadiusSquared);
		getAllies(sensorRadiusSquared);
	}
	
	public void run() throws GameActionException {
		
//		boolean nukeComing = bc.read(12325);
//		if (dest == null) {
//			Direction d = teamhq.directionTo(enemyhq);
//			if (bc.read(203)) {
//				d = d;
//			} else if (bc.read(204)) {
//				d = d.rotateLeft();
//			} else if (bc.read(205)) {
//				d = d.rotateRight();
//			} else if (bc.read(206)) {
//				d = d.rotateLeft().rotateLeft();
//			} else if (bc.read(207)) {
//				d = d.rotateRight().rotateRight();
//			} else if (bc.read(208)) {
//				d = d.rotateLeft().rotateLeft().rotateLeft();
//			} else if (bc.read(209)) {
//				d = d.rotateRight().rotateRight().rotateRight();
//			} else if (bc.read(210)) {
//				d = d.opposite();
//			} else {
////				d = null;
//			}
//			d = null;
//			if (d == null) {
//				dest = enemyhq;
//			} else {
//				int x = teamhq.x + d.dx*stray;
//				int y = teamhq.y + d.dy*stray;
//				if (x < 0) {
//					x = 0;
//				} else if (x >= width) {
//					x = width-1;
//				}
//				if (y < 0) {
//					y = 0;
//				} else if (y >= height) {
//					y = height-1;
//				}
//				dest = new MapLocation(x,y);
//			}
//		}
		dest = enemyhq;
		
		
		if (rc.isActive()) {
			if (nearbyEnemies.length > 0) {
				if (bugging){
					bugging = false;
					driller.startDrill(teamhq);
				}
				boolean stay = false;
				for (int i = 0; i < 8; i++) {
					if (enemies.contains(loc.add(Direction.values()[i])))
						stay = true;
				}
				if (rc.senseMine(loc) == team)
					stay = false;

				int activeEnemies = 0;
				for (RobotInfo rinfo : nearbyEnemies) {
					if (rinfo.roundsUntilAttackIdle == 0 && rinfo.roundsUntilMovementIdle == 0) {
						activeEnemies++;
					}
				}
				if (activeEnemies < nearbyAllies.length+2) {
					for (RobotInfo rinfo : nearbyEnemies) {
						if (rinfo.roundsUntilAttackIdle > 0 || rinfo.roundsUntilMovementIdle > 0) {
							Direction moveTo = loc.directionTo(rinfo.location);
							if (rc.canMove(moveTo)) {
								rc.move(moveTo);
								return;
							}
						}
					}
				}
				
				if (!stay) {
					doDrill();
				}

			} else {
				int doMine = 0;
				Direction[] dirs = new Direction[] { Direction.OMNI, Direction.NORTH,
						Direction.WEST, Direction.SOUTH, Direction.EAST};
				for (int i = 0; i < 5; i++) {
					if (rc.senseMine(loc.add(dirs[i])) == team)
						doMine++;
					if (rc.senseMine(loc.add(dirs[i])) == Team.NEUTRAL)
						doMine++;
				}
//				if(doMine < 3 && !nukeComing) {
//					rc.layMine();
//				} else {
					if (!bugging) {
						driller.startDrill(dest);
						bugging = true;
					}
					
					rc.setIndicatorString(0, dest.toString());
					doDrill();
//				}
			}
		}
		
		int startTime = Clock.getBytecodeNum();
		bc.read(12345, 55);
		int endTime = Clock.getBytecodeNum();
		rc.setIndicatorString(0, (endTime-startTime)+"");
	}
	
	public void doDrill() throws GameActionException{
		if (driller.next == null) {
			driller.drill(sensorRadiusSquared*4);

			if (driller.next != null) rc.setIndicatorString(1, driller.next.toString());
			else rc.setIndicatorString(1, "null");
		}
		
		if (driller.next == Direction.NONE) {
			driller.next = null;
		} else if (driller.next == Direction.OMNI) {
			Direction d = teamhq.directionTo(enemyhq);
			int x = teamhq.x + d.dx*stray;
			int y = teamhq.y + d.dy*stray;
			if (x < 0) {
				x = 0;
			} else if (x >= width) {
				x = width-1;
			}
			if (y < 0) {
				y = 0;
			} else if (y >= height) {
				y = height-1;
			}
//			if (x == dest.x && y == dest.y){
				dest = enemyhq;
//			} else {
//				dest = new MapLocation(x,y);
//			}
			driller.startDrill(dest);
		} else {
			if (driller.mine) {
				rc.defuseMine(loc.add(driller.next));
				driller.mine = false;
			} else {
				if (rc.canMove(driller.next)) {
					rc.move(driller.next);
					driller.next = null;
				}
			}
		}
	}
}
