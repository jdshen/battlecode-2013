package team102.robots;

import team102.utility.*;
import battlecode.common.*;
import team102.controller.*;

public class HQState extends SensingRobotState {
	private BroadcastController bc;
	private int count;
	private int delay;
	
	public HQState(RobotController rc) throws GameActionException {
		super(rc);
		bc = new BroadcastController(this);
		count = 0;
		delay = 0;
	}
	
	public void initSense() {
		getLocation();
		setEncampmentDelay(10);
		initUpgrades();
		initMines();
		initObjects();
	}
	
	public void updateSense() {
		getBasic();
		getEncampments();
		getUpgrades();
		getNumAllies();
		getAlliedMines(sensorRadiusSquared*2);
	}
	
	public void run() throws GameActionException {
		int directions = 5;
		bc.broadcast(12345, 55);
//		if (rc.senseEnemyNukeHalfDone()) {
//			bc.broadcast(12325,55);
//		} else if (count < directions) {
//			bc.broadcast(count+203);
//		} else if (count == directions){
//			bc.broadcast(0);
//		}
		if (rc.isActive() ) {
			int enemies = rc.senseNearbyGameObjects(Robot.class, this.sensorRadiusSquared*2, enemy).length;
			int numMines = nearbyAlliedMines.length;
//			if (count > 25 && (delay > 0 || numAllies > 6 || rc.checkResearchProgress(Upgrade.NUKE) > 340)) {
//				rc.researchUpgrade(Upgrade.NUKE);
//				if (delay > 0) delay--;
//			}
//			else if (count < directions || pickaxe) {
				delay = (numAllies+1)*(width/14+height/14)/2;
				Direction dir = Direction.values()[0];
				for (int i = 0; i < 8; i++) {
					if(rc.canMove(dir)) {
						rc.spawn(dir);
						count++;
						return;
					}
					dir = dir.rotateLeft();
				}
//			} else {
//				rc.researchUpgrade(Upgrade.PICKAXE);
//			}
		}
	}
}
