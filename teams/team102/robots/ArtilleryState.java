package team102.robots;

import battlecode.common.*;
import team102.controller.*;
import team102.utility.*;

public class ArtilleryState extends SensingRobotState{
	
	public ArtilleryState(RobotController rc) throws GameActionException {
		super(rc);
	}
	
	public void initSense() {
		
	}
	
	public void updateSense() {
		
	}
	
	public void run() throws GameActionException {
	}

}
