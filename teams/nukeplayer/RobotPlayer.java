package nukeplayer;

import team102.controller.*;
import team102.robots.*;
import battlecode.common.*;

public class RobotPlayer {
	public static void run(RobotController rc) {
		while (true) {
			try {
				if (rc.getType() == RobotType.HQ && rc.isActive()) {
					rc.researchUpgrade(Upgrade.NUKE);
				}
				rc.yield();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
