package team102.utility;
import battlecode.common.*;

public class FastLocSet {
    private static final int HASH = 3*Math.max(GameConstants.MAP_MAX_WIDTH, GameConstants.MAP_MAX_HEIGHT);
    private boolean[][] has = new boolean[HASH][HASH];

    public void add(MapLocation loc) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        if (!has[x][y]){
            has[x][y] = true;
        }
    }

    public void remove(MapLocation loc) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        if (has[x][y]){
            has[x][y] = false;
        }
    }

    public boolean contains(MapLocation loc) {
        return has[loc.x % HASH][loc.y % HASH];
    }

    public void clear() {
        has = new boolean[HASH][HASH];
    }
}
