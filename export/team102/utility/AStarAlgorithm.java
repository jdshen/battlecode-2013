package team102.utility;

import java.util.*;

import team102.controller.BasicRobotState;
import battlecode.common.*;


/**
 * Performs A* until hitting a point outside of the search radius (squared),
 * stores that point as mid, and
 * @author Jeffrey
 *
 */
public class AStarAlgorithm {
	private RobotController rc;
	public ArrayList<Direction> path;
	public int index;
	public MapLocation mid;
	
	public static final int INFINITY = 200000;
	
	public static final Direction[][] DIR = new Direction[][]{
		new Direction[]{Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST},
		new Direction[]{Direction.WEST, Direction.OMNI, Direction.EAST},
		new Direction[]{Direction.SOUTH_WEST,Direction.SOUTH,Direction.SOUTH_EAST}
	};
	
	public AStarAlgorithm (RobotController rc) {
		this.rc = rc;
	}
	
	public void compute(MapLocation from, MapLocation to, int radiusSquared,
			int height, int width, int defuseDelay, boolean[] mines) {
		
		int tox = to.x;
		int toy = to.y;
		
		boolean[] seen = new boolean[width*height];
		int[] g = new int[width*height];
		final int[] f = new int[width*height];
		int fromHash = from.x*height+from.y;
		g[fromHash] = -INFINITY;
		f[fromHash] = -INFINITY+from.distanceSquaredTo(to);
		MapLocation[] cameFrom = new MapLocation[width*height];
		
		//lol, space is cheap, and relatively fast, dont want to resize
		BinaryHeap<MapLocation> eval = new BinaryHeap<MapLocation>(height*width);
		eval.add(from,f[fromHash]);
		
		while (!eval.isEmpty()) {
			MapLocation cur = eval.poll();
			int curx = cur.x;
			int cury = cur.y;
			int curHash = height*curx + cury;
			//check if already seen, occurs since we put in duplicate maplocations
			if (seen[curHash]) continue;
			else seen[curHash] = true;
			
			//done
			if (cur.distanceSquaredTo(from) > radiusSquared || Clock.getBytecodesLeft() < 4000) {
				//final destination
				mid = cur;
				//find path
				path = new ArrayList<Direction>(radiusSquared);
				while (!cur.equals(from)) {
					MapLocation temp = cur;
					cur = cameFrom[temp.x*height+temp.y];
					path.add(cur.directionTo(temp));
				}
				
				rc.setIndicatorString(0, path.toString() + " " + path.size());
			    return;
			}
			
			int curG = g[curHash];
			
			//find neighbors
			int[] neighborsX;			
			int[] neighborsY;
			if (curx == 0) {
				if (cury == 0) {
					neighborsX = neighborsLUX;
					neighborsY = neighborsLUY;
				} else if (cury == width-1) {
					neighborsX = neighborsLDX;
					neighborsY = neighborsLDY;
				} else {
					neighborsX = neighborsLMX;
					neighborsY = neighborsLMY;
				}
			} else if (curx == width-1) {
				if (cury == 0) {
					neighborsX = neighborsRUX;
					neighborsY = neighborsRUY;
				} else if (cury == width-1) {
					neighborsX = neighborsRDX;
					neighborsY = neighborsRDY;
				} else {
					neighborsX = neighborsRMX;
					neighborsY = neighborsRMY;
				}
			} else {
				if (cury == 0) {
					neighborsX = neighborsMUX;
					neighborsY = neighborsMUY;
				} else if (cury == width-1) {
					neighborsX = neighborsMDX;
					neighborsY = neighborsMDY;
				} else {
					neighborsX = neighborsMMX;
					neighborsY = neighborsMMY;
				}
			}
			int end = neighborsX.length;

			//neighbors
			for (int i = 0; i < end; i++) {
				MapLocation neighbor = cur.add(neighborsX[i],neighborsY[i]);
				int neighborx = neighbor.x;
				int neighbory = neighbor.y;
				int neighborHash = height*neighborx + neighbory;
				if (seen[neighborHash])
					continue;

				//compute possible score
				int nextG = curG + 1;

				boolean isMine = mines[neighborHash];
				if (isMine) {
					nextG += defuseDelay;
				}

//				Team t = rc.senseMine(neighbor);
//				if (t != rc.getTeam() && t != null) {
//					nextG += defuseDelay;
//				}
				
				//add to eval
				if (nextG < g[neighborHash]) {
//					int startTime = Clock.getBytecodeNum();
					cameFrom[neighborHash] = cur;
//					int endTime = Clock.getBytecodeNum();
//					System.out.println(endTime-startTime);
					g[neighborHash] = nextG;
					f[neighborHash] = nextG + (Math.abs(tox- neighborx)+Math.abs(toy-neighbory))/2;
//					neighbor.distanceSquaredTo(to)
					eval.add(neighbor,f[neighborHash]);
				}
			}
			
		}

		rc.setIndicatorString(0, "bad");
	}
	
	/**
	 * Increment index
	 * @return return true if at end of path (i.e. index now is at least path.length)
	 */
	public boolean increment() {
		return ++index >= path.size();
	}
	/**
	 * Decrement index
	 * @return return true if at beginning of path (i.e. index is now negative)
	 */
	public boolean decrement() {
		return --index < 0;
	}
	
	
	private static final int[] neighborsMMX = new int[]{
		-1,-1,-1,0,0,1,1,1
	};
	private static final int[] neighborsMMY = new int[]{
		-1,0,1,-1,1,-1,0,1
	};

	private static final int[] neighborsLMX = new int[]{
		0,0,1,1,1
	};
	private static final int[] neighborsLMY = new int[]{
		-1,1,-1,0,1
	};

	
	private static final int[] neighborsRMX = new int[]{
		-1,-1,-1,0,0
	};
	private static final int[] neighborsRMY = new int[]{
		-1,0,1,-1,1
	};

	private static final int[] neighborsMUX = new int[]{
		-1,-1,0,1,1
	};
	private static final int[] neighborsMUY = new int[]{
		0,1,1,0,1
	};

	private static final int[] neighborsMDX = new int[]{
		-1,-1,0,1,1
	};
	private static final int[] neighborsMDY = new int[]{
		-1,0,-1,-1,0
	};

	private static final int[] neighborsLUX = new int[]{
		0,1,1
	};
	private static final int[] neighborsLUY = new int[]{
		1,0,1
	};

	private static final int[] neighborsLDX = new int[]{
		0,1,1
	};
	private static final int[] neighborsLDY = new int[]{
		-1,-1,0
	};

	private static final int[] neighborsRUX = new int[]{
		-1,-1,0
	};
	private static final int[] neighborsRUY = new int[]{
		0,1,1
	};
	
	private static final int[] neighborsRDX = new int[]{
		-1,-1,0
	};
	private static final int[] neighborsRDY = new int[]{
		-1,0,-1
	};
}
