package team102.robots;

import battlecode.common.*;
import team102.controller.*;

public class SoldierState extends Bugger {

	public SoldierState(RobotController rc) {
		super(rc);
	}
	
	public void initSense() {
		setEncampmentDelay(10);
		initUpgrades();
	}
	
	public void updateSense() {
		getLocation();
		getBasic();
		getEncampments();
		getUpgrades();
	}
	
	public void run() throws GameActionException {
		if (rc.isActive()) {
			if(rc.senseMine(rc.getLocation())==null) {
				rc.layMine();
			} else { 
				// Choose a random direction, and move that way if possible

				Direction dir = Direction.values()[(int)(Math.random()*8)];
				if (Math.random() < 0.7) {
					dir = loc.directionTo(enemyhq);
				}
				for (int i = 0; i < 20; i++) {
					if (rc.senseMine(loc.add(dir)) == null) {
						if(rc.canMove(dir)) {
							rc.move(dir);
							return;
						}
					} else if (rc.senseMine(loc.add(dir)) == Team.NEUTRAL) {
						rc.defuseMine(loc.add(dir));
						return;
					}
					if (Math.random() < 0.5) {
						dir = dir.rotateRight();
					} else dir = dir.rotateLeft();
				}
				for (int i = 0; i < 8; i++) {
					if(rc.canMove(dir) ) {
						if (rc.senseMine(loc.add(dir)) != Team.NEUTRAL) {
							rc.move(dir);
							return;
						}
					}
				}
			}
		}
	}
}
