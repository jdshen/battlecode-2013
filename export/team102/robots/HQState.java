package team102.robots;

import java.util.*;
import team102.utility.*;
import battlecode.common.*;
import team102.controller.*;

public class HQState extends SensingRobotState {
//	private Bugger b;
	
	private int count;
	public HQState(RobotController rc) {
		super(rc);
//		int startTime = Clock.getBytecodeNum();
//		b = new Bugger(rc);
//		int elapsed = Clock.getBytecodeNum()-startTime;
//		System.out.println(elapsed);
		count = 0;
	}
	
	public void initSense() {
		getLocation();
		setEncampmentDelay(10);
		initUpgrades();
	}
	
	public void updateSense() {
		getBasic();
		getEncampments();
		getUpgrades();
	}
	
	public void run() throws GameActionException {
		
		if (rc.isActive() ) {	
			if (count < 8) {
				Direction dir = Direction.values()[(int)(Math.random()*8)];
				for (int i = 0; i < 8; i++) {
					if(rc.canMove(dir)) {
						rc.spawn(dir);
						count++;
						return;
					}
					dir = dir.rotateLeft();
				}
				for (int i = 0; i < 8; i++) {
					if(rc.senseObjectAtLocation(loc.add(dir)) == null) {
						rc.spawn(dir);
						count++;
						return;
					}
					dir = dir.rotateLeft();
				}
			} else {
				rc.researchUpgrade(Upgrade.NUKE);
			}
		}
	}
}
