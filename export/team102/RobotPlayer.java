package team102;

import team102.controller.*;
import team102.robots.*;
import battlecode.common.*;

public class RobotPlayer {
	public static void run(RobotController rc) {
		SensingRobotState rs = null;
		do {
			try {
				switch (rc.getType()) {
				case HQ: rs = new HQState(rc);
					break;
				case SOLDIER:rs = new SoldierState(rc);
					break;
				case ARTILLERY: rs = new ArtilleryState(rc);
					break;
				case GENERATOR: rs = new GeneratorState(rc);			
					break;
				case MEDBAY: rs = new MedBayState(rc);		
					break;
				case SHIELDS: rs = new ShieldsState(rc);
					break;
				case SUPPLIER: rs = new SupplierState(rc);
					break;
				default:
					rs = null;
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (rs == null);
		
		while (true) {
			try {
				rs.updateSense();
				rs.run();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rc.yield();
		}
	}
}
