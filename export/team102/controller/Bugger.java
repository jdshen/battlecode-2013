package team102.controller;

import java.util.ArrayList;

import team102.controller.*;
import team102.utility.FastLocSet;
import battlecode.common.*;

/**
 * A bugger that looks ahead two squares (willing to go through one mine, and then keep bugging).
 * @author Jeffrey
 *
 */
public abstract class Bugger extends SensingRobotState{
	
	public static enum HugState {
		MOVE, HUG_ONE, HUG_TWO, DRILL_ONE, DRILL_TWO
	}
	
	private HugState hugging; //0 if not hugging, 1 if hugging one dir, 2 if hugging second
	private boolean hugLeft; //which way currently hugging
	private FastLocSet seen;
	private MapLocation mid;
	private MapLocation to;
	private MapLocation hug;
	private Direction desiredDir;

	protected MapLocation from; //original starting point
	protected Direction next;
	protected boolean mine;
	
	private static final Direction[][] DIR = new Direction[][]{
		new Direction[]{Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST},
		new Direction[]{Direction.WEST, Direction.OMNI, Direction.EAST},
		new Direction[]{Direction.SOUTH_WEST,Direction.SOUTH,Direction.SOUTH_EAST}
	};
	
	private static final Direction[][] BEST_DIR = getBestDir();
	
	private static final SecondStep[][] BEST_2STEP = getBestSecondSteps();
			
	private static class SecondStep {
		public final int x;
		public final int y;
		public final Direction[] d;
		public SecondStep(int x, int y, Direction[] d) {
			this.x = x;
			this.y = y;
			this.d = d;
		}
		
		@Override
		public String toString() {
			String s = "";
			for (Direction dir : d) {
				s += dir+",";
			}
			
			return x + ","+ y + ";" + s;
		}
	}
	
	
	public Bugger(RobotController rc) {
		super(rc);
	}
	
	public void startBug(MapLocation from, MapLocation to) {
		this.from = from;
		this.mid = from;
		this.to = to;
		hugging = HugState.MOVE;
		hug = null;
		hugLeft = true;
		seen = new FastLocSet();
	}
	
	/**
	 * Uses bugging algorithm to find next move, tells you if your next move goes through a mine
	 * .next stores next move, .mine is true iff there is a mine 
	 * @paam loc current location
	 */
	public void bug() {
		desiredDir = loc.directionTo(to);
		if (desiredDir == Direction.NONE || desiredDir == Direction.OMNI) {
			next = desiredDir;
			mine = false;
			return;
		}
		
		switch(hugging) {
		case MOVE:
			if (checkMove(loc)) return;
			break;
		case HUG_ONE:
		}

//		//check if you can go in a good direction
//		//check if you can go in a good direction, through a mine
//		
//		if (hugging == 2) {
//			if (seen.contains(loc)) {
//				
//			}
//		} else if (hugging == 1) {
//			
//		}
//		if (hugging > 0) {
//			if (seen.contains(loc) && hugLeft) {
//				hugging = 0;
//			}
//
//			
//			seen.add(loc);
//		}
//		
//		if (!hugging)
//		{
//			Direction bestDir = goInDir(desiredDir);
//			if (bestDir != null)
//	            return bestDir;
//			
//			seen.clear();
//			hugging = true;
//			mid = loc;
//			hugDir = desiredDir;
//			recursed = false;
//			return hug();
//		}
//		else
//		{
//			return hug();
//		}
	}
	
	public boolean checkMove(MapLocation loc) {
		//check one step ahead
		Direction[] best = BEST_DIR[desiredDir.ordinal()];
		for (Direction d : best) {
			if (rc.canMove(d)) {
				next = d;
				mine = false;
				return true;
			}
		}
		
		//check two steps
		SecondStep[] secondStep = BEST_2STEP[desiredDir.ordinal()];
		for (SecondStep s : secondStep) {
//			if ( != null){
//				
//			}
			
			for (Direction dir : s.d) {
				Team t = rc.senseMine(loc.add(dir));
				if (t == enemy || t == null) {
					next = dir;
					mine = true;
					return true;
				}
			}
		}
		return false;
	}
	
//	public boolean checkHugOne() {
////		&& loc.distanceSquaredTo(to) <= mid.distanceSquaredTo(to)
//	}
//	
//	public boolean checkHugTwo() {
//		
//	}
	
	private static Direction[][] getBestDir() {
		Direction[][] ret = new Direction[Direction.values().length][];
		for (Direction d : Direction.values()) {
			Direction[] best = new Direction[3];
			best[0] = d;
			best[1] = d.rotateLeft();
			best[2] = d.rotateRight();
			ret[d.ordinal()] = best;
		}
		return ret;
	}
	
	private static SecondStep[][] getBestSecondSteps() {
		SecondStep[][] list = new SecondStep[Direction.values().length][];
		SecondStep[] s = new SecondStep[16];
		s[0] = new SecondStep(-2,-2,new Direction[]{Direction.NORTH_WEST});
		s[1] = new SecondStep(-1,-2,new Direction[]{Direction.NORTH_WEST, Direction.NORTH});
		s[2] = new SecondStep(0,-2,new Direction[]{Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST});
		s[3] = new SecondStep(1,-2,new Direction[]{Direction.NORTH, Direction.NORTH_EAST});
		s[4] = new SecondStep(2,-2,new Direction[]{Direction.NORTH_EAST});
		s[5] = new SecondStep(2,-1,new Direction[]{Direction.NORTH_EAST, Direction.EAST});
		s[6] = new SecondStep(2,0,new Direction[]{Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST});
		s[7] = new SecondStep(2,1,new Direction[]{Direction.EAST, Direction.SOUTH_EAST});
		s[8] = new SecondStep(2,2,new Direction[]{Direction.SOUTH_EAST});
		s[9] = new SecondStep(1,2,new Direction[]{Direction.SOUTH_EAST, Direction.SOUTH});
		s[10] = new SecondStep(0,2,new Direction[]{Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST});
		s[11] = new SecondStep(-1,2,new Direction[]{Direction.SOUTH, Direction.SOUTH_WEST});
		s[12] = new SecondStep(-2,2,new Direction[]{Direction.SOUTH_WEST});
		s[13] = new SecondStep(-2,1,new Direction[]{Direction.SOUTH_WEST, Direction.WEST});
		s[14] = new SecondStep(-2,0,new Direction[]{Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST});
		s[15] = new SecondStep(-2,-1,new Direction[]{Direction.WEST, Direction.NORTH_WEST});

		//for each possible direction
		int best = 2; //north is first
		for (Direction d : Direction.values()) {
			if (d == Direction.OMNI || d == Direction.NONE) {
				list[d.ordinal()] = new SecondStep[0];
				continue;
			}
			//add up every pair of directions and add to secondStep
			SecondStep[] secondStep = new SecondStep[5];
			secondStep[0] = s[best%16];
			secondStep[1] = s[(best+1)%16];
			secondStep[2] = s[(best-1)%16];
			secondStep[3] = s[(best+2)%16];
			secondStep[4] = s[(best-2)%16];
			best+=2;
			list[d.ordinal()] = secondStep;
		}
		return list;
	}
	
	@Override
	protected abstract void initSense();

	@Override
	public abstract void updateSense();

	@Override
	public abstract void run() throws GameActionException;
	
	public static void main(String[] args) {
		for (int i = 0; i < Direction.values().length; i++) {
			Direction d = Direction.values()[i];
			SecondStep[] arr = BEST_2STEP[d.ordinal()];
			for (SecondStep s : arr) {
				System.out.println(d+ " " + s);
			}
		}
		
		Direction d= Direction.EAST;
		System.out.println(BEST_2STEP[d.ordinal()][2].x + " "+BEST_2STEP[d.ordinal()][2].y);
	}

}
