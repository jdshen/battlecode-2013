package team102.controller;
import battlecode.common.*;

public abstract class BasicRobotState {
	//shortcuts for basic information
	public final int width;
	public final int height;
	public final Team team;
	public final Team enemy;
	public final MapLocation teamhq;
	public final MapLocation enemyhq;
	protected final RobotController rc;
	public final Robot robot;
	public final RobotType type;
	
	//shortcuts for type
	public final double attackPower;
	public final int attackDelay;
	public final double attackRadiusMaxSquared;
	public final double maxEnergon;	
	
	public BasicRobotState(RobotController rc) {
		width = rc.getMapWidth();
		height = rc.getMapHeight();
		team = rc.getTeam();
		enemy = team.opponent();
		teamhq = rc.senseHQLocation();
		enemyhq = rc.senseEnemyHQLocation();
		robot = rc.getRobot();
		type = rc.getType();
		this.rc = rc;
		attackPower = type.attackPower;
		attackDelay = type.attackDelay;
		attackRadiusMaxSquared = type.attackRadiusMaxSquared;
		maxEnergon = type.maxEnergon;
	}
	
	public abstract void run() throws GameActionException;
}
